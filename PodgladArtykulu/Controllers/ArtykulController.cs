﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PodgladArtykulu.Model;

namespace PodgladArtykulu.Controllers
{
    public static class ArtykulController
    {
        public static ArtykulModel PobierzArtykul()
        {
            ArtykulModel artykul;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                artykul = (from a in entity.ARTYKUL
                           where a.ID_ARTYKULU == ParametryUruchomienioweController.idArtykulu
                           select new ArtykulModel()
                           {
                               ID_ARTYKULU = a.ID_ARTYKULU,
                               INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY,
                               NAZWA = a.NAZWA
                           }).FirstOrDefault();
            }

            return artykul;
        }
    }
}
