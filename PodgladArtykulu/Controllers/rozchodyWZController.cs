﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PodgladArtykulu.Model;

namespace PodgladArtykulu.Controllers
{
    class RozchodyWZController
    {
        public static List<OperacjaModel> PobierzOperacjeWZ()
        {
            List<OperacjaModel> operacjeWZ;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                operacjeWZ = (from p in entity.C_imag_operacje_dok_mag_rozchodowe_wms
                             join pz in entity.POZYCJA_ZAMOWIENIA on p.ID_POZ_DOK_MAG equals pz.ID_POZYCJI_ZAMOWIENIA
                             join z in entity.ZAMOWIENIE on p.ID_DOK_MAGAZYNOWEGO equals z.ID_ZAMOWIENIA
                             where pz.ID_ARTYKULU == ParametryUruchomienioweController.idArtykulu
                             orderby z.NUMER ascending
                             select new OperacjaModel()
                             {
                                 ilosc = p.ilosc,
                                 NUMER = z.NUMER,
                                 REFNO = p.REFNO,
                                 TYP_OPERACJI = p.TYP_OPERACJI
                             }
                          ).ToList();
            }

            return operacjeWZ;
        }
    }
}
