﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PodgladArtykulu.Model;

namespace PodgladArtykulu.Controllers
{
    public static class PartyController
    {
        public static List<PartModel> PobierzParty()
        {
            List<PartModel> party;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                party = (from s in entity.vStany_WMS
                         join k in entity.KONTRAHENT on (decimal)s.idKontrahenta equals k.ID_KONTRAHENTA
                         join l in entity.vLokalizacje on s.idLokalizacji equals l.idLokalizacji
                         where s.idArtykulu == ParametryUruchomienioweController.idArtykulu
                         orderby k.NAZWA
                         select new PartModel()
                         {
                             lokalizacja = l.lokalizacja,
                             NAZWA = k.NAZWA,
                             STAN = s.STAN
                         }
                          ).ToList();
            }

            return party;
        }
    }
}
