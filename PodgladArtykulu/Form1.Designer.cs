﻿namespace PodgladArtykulu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.tabControl_szczegulyArtykulu = new System.Windows.Forms.TabControl();
            this.tabPage_party = new System.Windows.Forms.TabPage();
            this.dataGridView_party = new System.Windows.Forms.DataGridView();
            this.tabPage_przychody = new System.Windows.Forms.TabPage();
            this.dataGridView_przychody = new System.Windows.Forms.DataGridView();
            this.tabPage_rozchodyWZ = new System.Windows.Forms.TabPage();
            this.dataGridView_rozchodyWZ = new System.Windows.Forms.DataGridView();
            this.tabPage_rozchodyKompletacja = new System.Windows.Forms.TabPage();
            this.dataGridView_rozchodyKompletacja = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.tabControl_szczegulyArtykulu.SuspendLayout();
            this.tabPage_party.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_party)).BeginInit();
            this.tabPage_przychody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_przychody)).BeginInit();
            this.tabPage_rozchodyWZ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozchodyWZ)).BeginInit();
            this.tabPage_rozchodyKompletacja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozchodyKompletacja)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 371);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(673, 69);
            this.panel1.TabIndex = 0;
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(569, 20);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // tabControl_szczegulyArtykulu
            // 
            this.tabControl_szczegulyArtykulu.Controls.Add(this.tabPage_party);
            this.tabControl_szczegulyArtykulu.Controls.Add(this.tabPage_przychody);
            this.tabControl_szczegulyArtykulu.Controls.Add(this.tabPage_rozchodyWZ);
            this.tabControl_szczegulyArtykulu.Controls.Add(this.tabPage_rozchodyKompletacja);
            this.tabControl_szczegulyArtykulu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_szczegulyArtykulu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_szczegulyArtykulu.Name = "tabControl_szczegulyArtykulu";
            this.tabControl_szczegulyArtykulu.SelectedIndex = 0;
            this.tabControl_szczegulyArtykulu.Size = new System.Drawing.Size(673, 371);
            this.tabControl_szczegulyArtykulu.TabIndex = 1;
            // 
            // tabPage_party
            // 
            this.tabPage_party.Controls.Add(this.dataGridView_party);
            this.tabPage_party.Location = new System.Drawing.Point(4, 22);
            this.tabPage_party.Name = "tabPage_party";
            this.tabPage_party.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_party.Size = new System.Drawing.Size(665, 345);
            this.tabPage_party.TabIndex = 0;
            this.tabPage_party.Text = "Party";
            this.tabPage_party.UseVisualStyleBackColor = true;
            // 
            // dataGridView_party
            // 
            this.dataGridView_party.AllowUserToAddRows = false;
            this.dataGridView_party.AllowUserToDeleteRows = false;
            this.dataGridView_party.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_party.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_party.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_party.Name = "dataGridView_party";
            this.dataGridView_party.ReadOnly = true;
            this.dataGridView_party.Size = new System.Drawing.Size(659, 339);
            this.dataGridView_party.TabIndex = 0;
            this.dataGridView_party.DataSourceChanged += new System.EventHandler(this.dataGridView__DataSourceChanged);
            this.dataGridView_party.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_party_ColumnAdded);
            // 
            // tabPage_przychody
            // 
            this.tabPage_przychody.Controls.Add(this.dataGridView_przychody);
            this.tabPage_przychody.Location = new System.Drawing.Point(4, 22);
            this.tabPage_przychody.Name = "tabPage_przychody";
            this.tabPage_przychody.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_przychody.Size = new System.Drawing.Size(665, 345);
            this.tabPage_przychody.TabIndex = 1;
            this.tabPage_przychody.Text = "Przychody";
            this.tabPage_przychody.UseVisualStyleBackColor = true;
            // 
            // dataGridView_przychody
            // 
            this.dataGridView_przychody.AllowUserToAddRows = false;
            this.dataGridView_przychody.AllowUserToDeleteRows = false;
            this.dataGridView_przychody.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_przychody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_przychody.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_przychody.Name = "dataGridView_przychody";
            this.dataGridView_przychody.ReadOnly = true;
            this.dataGridView_przychody.Size = new System.Drawing.Size(659, 339);
            this.dataGridView_przychody.TabIndex = 0;
            this.dataGridView_przychody.DataSourceChanged += new System.EventHandler(this.dataGridView__DataSourceChanged);
            // 
            // tabPage_rozchodyWZ
            // 
            this.tabPage_rozchodyWZ.Controls.Add(this.dataGridView_rozchodyWZ);
            this.tabPage_rozchodyWZ.Location = new System.Drawing.Point(4, 22);
            this.tabPage_rozchodyWZ.Name = "tabPage_rozchodyWZ";
            this.tabPage_rozchodyWZ.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_rozchodyWZ.Size = new System.Drawing.Size(665, 345);
            this.tabPage_rozchodyWZ.TabIndex = 2;
            this.tabPage_rozchodyWZ.Text = "Rozchody WZ";
            this.tabPage_rozchodyWZ.UseVisualStyleBackColor = true;
            // 
            // dataGridView_rozchodyWZ
            // 
            this.dataGridView_rozchodyWZ.AllowUserToAddRows = false;
            this.dataGridView_rozchodyWZ.AllowUserToDeleteRows = false;
            this.dataGridView_rozchodyWZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_rozchodyWZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_rozchodyWZ.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_rozchodyWZ.Name = "dataGridView_rozchodyWZ";
            this.dataGridView_rozchodyWZ.ReadOnly = true;
            this.dataGridView_rozchodyWZ.Size = new System.Drawing.Size(659, 339);
            this.dataGridView_rozchodyWZ.TabIndex = 0;
            this.dataGridView_rozchodyWZ.DataSourceChanged += new System.EventHandler(this.dataGridView__DataSourceChanged);
            // 
            // tabPage_rozchodyKompletacja
            // 
            this.tabPage_rozchodyKompletacja.Controls.Add(this.dataGridView_rozchodyKompletacja);
            this.tabPage_rozchodyKompletacja.Location = new System.Drawing.Point(4, 22);
            this.tabPage_rozchodyKompletacja.Name = "tabPage_rozchodyKompletacja";
            this.tabPage_rozchodyKompletacja.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_rozchodyKompletacja.Size = new System.Drawing.Size(665, 345);
            this.tabPage_rozchodyKompletacja.TabIndex = 3;
            this.tabPage_rozchodyKompletacja.Text = "Rozchody Kompletacja";
            this.tabPage_rozchodyKompletacja.UseVisualStyleBackColor = true;
            // 
            // dataGridView_rozchodyKompletacja
            // 
            this.dataGridView_rozchodyKompletacja.AllowUserToAddRows = false;
            this.dataGridView_rozchodyKompletacja.AllowUserToDeleteRows = false;
            this.dataGridView_rozchodyKompletacja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_rozchodyKompletacja.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_rozchodyKompletacja.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_rozchodyKompletacja.Name = "dataGridView_rozchodyKompletacja";
            this.dataGridView_rozchodyKompletacja.ReadOnly = true;
            this.dataGridView_rozchodyKompletacja.Size = new System.Drawing.Size(659, 339);
            this.dataGridView_rozchodyKompletacja.TabIndex = 0;
            this.dataGridView_rozchodyKompletacja.DataSourceChanged += new System.EventHandler(this.dataGridView__DataSourceChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 440);
            this.Controls.Add(this.tabControl_szczegulyArtykulu);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Podgląd artykulu";
            this.panel1.ResumeLayout(false);
            this.tabControl_szczegulyArtykulu.ResumeLayout(false);
            this.tabPage_party.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_party)).EndInit();
            this.tabPage_przychody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_przychody)).EndInit();
            this.tabPage_rozchodyWZ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozchodyWZ)).EndInit();
            this.tabPage_rozchodyKompletacja.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozchodyKompletacja)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl_szczegulyArtykulu;
        private System.Windows.Forms.TabPage tabPage_party;
        private System.Windows.Forms.TabPage tabPage_przychody;
        private System.Windows.Forms.TabPage tabPage_rozchodyWZ;
        private System.Windows.Forms.TabPage tabPage_rozchodyKompletacja;
        private System.Windows.Forms.DataGridView dataGridView_party;
        private System.Windows.Forms.DataGridView dataGridView_przychody;
        private System.Windows.Forms.DataGridView dataGridView_rozchodyWZ;
        private System.Windows.Forms.DataGridView dataGridView_rozchodyKompletacja;
        private System.Windows.Forms.Button button_zamknij;
    }
}

