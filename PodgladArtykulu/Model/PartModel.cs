﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PodgladArtykulu.Model
{
    public class PartModel
    {
        /// <summary>
        /// Z widoku vStany_WMS.
        /// </summary>
        public Nullable<decimal> STAN { get; set; }

        /// <summary>
        /// Z widoku vLokalizacje.
        /// </summary>
        public string lokalizacja { get; set; }

        /// <summary>
        /// Z tabeli KONTRAHENT.
        /// </summary>
        public string NAZWA { get; set; }
    }
}
