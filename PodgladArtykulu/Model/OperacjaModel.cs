﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PodgladArtykulu.Model
{
    public class OperacjaModel
    {
        /// <summary>
        /// Z tabelki C_imag_operacje_dok_mag_przychodowe_wms.
        /// </summary>
        public long REFNO { get; set; }
        public string TYP_OPERACJI { get; set; }
        public decimal ilosc { get; set; }

        /// <summary>
        /// Z tabelki ZAMOWIENIE.
        /// </summary>
        public string NUMER { get; set; }
    }
}
