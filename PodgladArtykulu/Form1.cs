﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PodgladArtykulu.Controllers;
using PodgladArtykulu.Model;

namespace PodgladArtykulu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ZaladujDaneNaFormularz();
        }

        private void ZaladujDaneNaFormularz()
        {
            ZaladujTytulOkna();
            ZaladujPartyDoSiatki();
            ZaladujPrzyjeciaDoSiatki();
            ZaladujOperacjeWZDoSiatki();
            ZaladujOperacjeKompletacjiDoSiatki();
        }

        private void ZaladujTytulOkna()
        {
            ArtykulModel artykul = ArtykulController.PobierzArtykul();
            if (artykul != null)
            {
                this.Text = $@"Artykul: {artykul.NAZWA}, o indeksie katalogowym: {artykul.INDEKS_KATALOGOWY}";
            }
        }

        private void ZaladujPartyDoSiatki()
        {
            List<PartModel> party = PartyController.PobierzParty();
            dataGridView_party.DataSource = party;
        }

        private void ZaladujPrzyjeciaDoSiatki()
        {
            List<OperacjaModel> przyjecia = PrzyjeciaController.PobierzPrzyjecia();
            dataGridView_przychody.DataSource = przyjecia;
        }

        private void ZaladujOperacjeWZDoSiatki()
        {
            List<OperacjaModel> operacjeWZ = RozchodyWZController.PobierzOperacjeWZ();
            dataGridView_rozchodyWZ.DataSource = operacjeWZ;
        }

        private void ZaladujOperacjeKompletacjiDoSiatki()
        {
            List<OperacjaModel> operacjeKompletacji = RozchodyKompletacjaController.PobierzOperacjeKompletacji();
            dataGridView_rozchodyKompletacja.DataSource = operacjeKompletacji;
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView__DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_party_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "STAN":
                    e.Column.Visible = true;
                    break;
                case "lokalizacja":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "PART";
                    break;
                case "NAZWA":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "KONTRAHENT";
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }
    }
}
